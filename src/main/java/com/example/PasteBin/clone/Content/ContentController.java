package com.example.PasteBin.clone.Content;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

import static org.springframework.boot.SpringApplication.exit;

@Controller

public class ContentController {
    private final ContentService contentService;

    @Autowired
    public ContentController(ContentService contentService) {
        this.contentService = contentService;

    }

   @GetMapping("/")
   public String contentForm(Model model) {
       model.addAttribute("search", new Content());
       model.addAttribute("submit", new Content());
       return "content";
   }

    @PostMapping("/submit")
    public String contentSubmit(@ModelAttribute Content content, Model model) {
        model.addAttribute("submit", content);
        if ((content.getUsername() == "") || (content.getTitle() == "") || (content.getText() == "")) {
            return "contentEmpty";
        } else {
            contentService.addContent(content);
        }
        return "submit";
    }

    @GetMapping("/search")
    public String contentSearch(@ModelAttribute Content username, Model model) {
        model.addAttribute("search", username);
       if (username.getUsername() == "") {
           return "searchError";
       } else {
           List<Content> contentReceived = contentService.getContent(username.getUsername());
           model.addAttribute("contentReceived", contentReceived);
           if (contentReceived.isEmpty()) {
                return "searchNoUser";
           }
       }
       return "search";
    }
}
