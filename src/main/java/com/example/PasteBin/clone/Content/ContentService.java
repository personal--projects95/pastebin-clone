package com.example.PasteBin.clone.Content;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.List;

@Service
public class ContentService {
    private final ContentRepository contentRepository;
    @Autowired
    public ContentService(ContentRepository contentRepository) {
        this.contentRepository = contentRepository;
    }

    public List<Content> getContent(String username) {
        return contentRepository.findByUsername(username);
    }

    public void addContent (Content content) {
        content.setDate(LocalDateTime.now());
        contentRepository.save(content);
    }

    public List<Content> allContent(){
        return contentRepository.findAll();
    }
}
