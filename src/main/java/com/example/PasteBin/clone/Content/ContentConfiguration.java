package com.example.PasteBin.clone.Content;


import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.List;

@Configuration
public class ContentConfiguration  {

    @Bean
    CommandLineRunner commandLineRunner(ContentRepository repository) {
        return args -> {
            Content content1 = new Content(
                    "user1",
                    "text1",
                    LocalDateTime.now(),
                    "title1"
            );
            Content content2 = new Content(
                    "user2",
                    "tex2",
                    LocalDateTime.now(),
                    "title2"
            );
            Content content3 = new Content(
                    "user1",
                    "text3",
                    LocalDateTime.now(),
                    "title3"
            );


            //repository.saveAll(List.of(content1, content2, content3));

        };
    }
}
