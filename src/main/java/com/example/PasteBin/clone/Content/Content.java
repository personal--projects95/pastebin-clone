package com.example.PasteBin.clone.Content;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
@Entity
@Table
public class Content {
    @Id
    @SequenceGenerator(
            name = "content_sequence",
            sequenceName = "content_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "content_sequence"
    )
    private Long id;
    private String username;
    private String text;
    private LocalDateTime date;
    private String title;

   public Content() {
    }

    public Content(Long id, String username, String text, LocalDateTime date, String title) {
       this.id = id;
       this.username = username;
       this.text = text;
       this.date = date;
       this.title = title;
    }

    public Content(String username, String text, LocalDateTime date, String title) {
       this.username = username;
       this.text = text;
       this.date = date;
       this.title = title;
    }
    public String getTitle() {
       return title;
    }

    public void setTitle(String title){
       this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
       this.id = id;
    }

    public String getUsername() {
       return username;
    }

    public void setUsername(String username) {
       this.username = username;
    }

    public String getText() {
       return text;
    }

    public void setText(String text) {
       this.text = text;
    }

    public LocalDateTime getDate() {
       return date;
    }

    public void setDate(LocalDateTime date) {
       this.date = date;
    }

    @Override
    public String toString() {
        return "Content{" +
                "username='" + username + '\'' +
                ", text='" + text + '\'' +
                ", date=" + date +
                '}';
    }
}
